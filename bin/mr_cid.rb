#!/usr/bin/env ruby
# frozen_string_literal: true

require 'require_all'
require_rel '../lib'

options = Options.parse!

DetectorService.run(options)
