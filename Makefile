.PHONY: test
test: rspec

.PHONY: lint
lint: rubocop

.PHONY: bundle-install
bundle-install:
	bundle check || bundle install

.PHONY: rspec
rspec: bundle-install
	bundle exec rspec

.PHONY: rubocop
rubocop: bundle-install
	bundle exec rubocop
