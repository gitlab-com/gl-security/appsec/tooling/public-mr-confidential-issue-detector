# Mr. Cid - MR Confidential Issue Detector

GitLab's Application Security Team have a responsibility to review MRs (which are always public) that reference confidential issues. The goal is to reduce the amount of time a security patch might be inadvertently public. Security patches should be made against a security mirror.

The Product Security Engineering team are responsible for maintaining this project. All are very welcome to contribute!

## CI Job

This project's CI feature is only visible to project members - this is a security precaution in case the Output To Console feature is used during a job. It is run on a schedule across multiple projects and can be viewed at https://gitlab.com/gitlab-com/gl-security/product-security/appsec/tooling/public-mr-confidential-issue-detector/-/pipeline_schedules.

The PAT in use is configured at Settings > CI/CD > Variables > `GITLAB_API_PRIVATE_TOKEN`.

If the bot runs amok, making the scheduled job inactive is the first place to start.

## Usage

- Create or use an account that has at least Reporter permissions for the target project(s)
- Create a PAT that has `read_api` access.

```shell
% GITLAB_API_PRIVATE_TOKEN=$(op item get "Mr Cid" --fields="credential") \
SLACK_BOT_OAUTH_TOKEN=$(op item get "Slack App Credentials" --fields="bot_token") \
./bin/mr_cid.rb \
  --since="2023-10-19T00:00Z" \
  --project_path="gitlab-com/gl-security/product-security/appsec/tooling/confidential-issue-detector-test-project/project" \
  --outputs=console,slack \
  --slack_channel=C0646GHD2BS
```

Multiple project paths are supported with comma-delimited values, e.g.:

```shell
./bin/mr_cid.rb \
  --project_path="gitlab-org/gitlab,gitlab-org/gitaly" \
```

Example output:
```
Starting scan at 2023-10-19 23:49:58 UTC.
========================================
Title:  Reference a confidential issue in the description
URL:    https://gitlab.com/gitlab-com/gl-security/product-security/appsec/tooling/confidential-issue-detector-test-project/project/-/merge_requests/3
Opened: 2023-10-19T23:19:34.871Z

Confidential issues detected:
  Title:        Confidential bug that shouldn't be disclosed
  URL:          https://gitlab.com/gitlab-com/gl-security/product-security/appsec/tooling/confidential-issue-detector-test-project/project/-/issues/5
  Confidential: true
  Labels:       ["bug::vulnerability", "securitybot::ignore", "severity::1"]

========================================
Title:  Reference a confidential issue in the branch name
URL:    https://gitlab.com/gitlab-com/gl-security/product-security/appsec/tooling/confidential-issue-detector-test-project/project/-/merge_requests/1
Opened: 2023-10-19T23:15:43.121Z

Confidential issues detected:
  Title:        Confidential bug that shouldn't be disclosed
  URL:          https://gitlab.com/gitlab-com/gl-security/product-security/appsec/tooling/confidential-issue-detector-test-project/project/-/issues/5
  Confidential: true
  Labels:       ["bug::vulnerability", "securitybot::ignore", "severity::1"]

Done. Reviewed 3 issues in 4.383477s.
```

## Testing

### Test/RSpec

`make test`

### Linting/RuboCop

`make lint`

## Caching

Mr CID uses two caches, detailed below. Keys are always strings, never symbolized.

### `cache/SCAN_HISTORY.json`

This file is formatted `{NAMESPACED_PROJECT: SINCE}`.

By default, if no SINCE argument is passed for a known `NAMESPACED_PROJECT`, the `SINCE` will be taken from the cache and updated at the end of a successful run.


### `cache/ALERT_HISTORY.json`

This file is formatted `{NAMESPACED_PROJECT_MR: {OUTPUT_LOCATION: TIMESTAMP}}`.

If a successful alert is sent (e.g. to Slack), it will be added to the cache.

Outputs to the console are never cached.

## Threat Model

### What are we building

```mermaid
sequenceDiagram
    participant tool as Mr CID
    participant gitlab as GitLab API
    participant slack as Slack
    participant appsec as AppSec Engineer

    Note over tool, gitlab: As a scheduled CI job
    tool->>gitlab: GET merge_requests
    gitlab->>tool: response
    loop With merge requests
        tool->>tool: Extract issue references
        loop With issue IDs
            tool->>gitlab: Get issue
            gitlab->>tool: responses
            tool->>tool: Check confidentiality
            tool->>tool: Check issue labels
        end
        alt Confidential issue(s) found
            tool->>tool: Check cache/ALERT_HISTORY.json
            tool->>tool: Build alert content

            Note over tool,slack: If configured
            tool->>slack: Alert to slack channel
        end
    end

    Note over slack,appsec: This task is assigned <br /> on a weekly rotation
    appsec->>slack: Check alerts
```

### What could go wrong, what are we going to do about it?

| Risk | Controls |
|------|----------|
| The PAT for this automation leaks | &bull; A dedicated service account with least access is used<br /> &bull; A readonly token is used |
| The automation fails and we aren't alerted | &bull; Graceful retries & errors <br /> &bull; **TODO** [Standalone pipeline monitoring / webhooks](https://gitlab.com/gitlab-com/gl-security/product-security/appsec/tooling/public-mr-confidential-issue-detector/-/issues/14) |
| False positives lead to ignoring the alerts, OR False negatives lead to missing an alert | &bull; Refined ruleset with robust tests <br /> &bull; Caching to avoid duplicates |
| Non-team members made aware of MRs | &bull; Project CI/CD is set to "Members Only" |
| Malicious MR parsed by tool | &bull; Ruby 3.2+ has ReDoS protection <br /> &bull; User-controlled values only used in JSON values, not as arguments to anything spicy <br /> &bull; SAST |
