# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Cache::Base do
  subject(:cache) { described_class.new(test_mode:) }

  let(:test_mode) { false }
  let(:cache_location) { 'cache/test.json' }
  let(:key) { 'example-key' }
  let(:value) { { 'expected_value' => [1, 2, { 'three' => 'four' }] } }
  let(:value_as_json) { '{"example-key":{"expected_value":[1,2,{"three":"four"}]}}' }

  before do
    stub_const('Cache::Base::CACHE_FILENAME', cache_location)
  end

  it 'adds values to a hash' do
    cache.add(key, value)
    expect(cache.value_for('example-key')).to eq(value)
  end

  it 'reads existing cache if it exists' do
    allow(File).to receive_messages(exist?: true, read: value_as_json)

    expect(cache.value_for(key)).to eq(value)
  end

  describe '#flush' do
    subject(:flush) { cache.flush }

    before do
      cache.add(key, value)
      allow(File).to receive(:write).and_return(true)
    end

    it 'writes JSON to the expected file' do
      flush
      expect(File).to have_received(:write).with(cache_location, value_as_json)
    end

    it 'returns true' do
      expect(flush).to be true
    end

    context 'when test mode is true' do
      let(:test_mode) { true }

      it 'does not write the json to a file' do
        flush
        expect(File).not_to have_received(:write)
      end

      it 'returns false' do
        expect(flush).to be false
      end
    end
  end

  describe '#cache' do
    it 'uses a class var' do
      original = described_class.cache
      expect(described_class.instance_variable_get('@cache')).to eq(original)
    end
  end
end
