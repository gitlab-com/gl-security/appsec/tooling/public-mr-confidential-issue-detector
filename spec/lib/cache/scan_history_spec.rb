# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Cache::ScanHistory do
  subject(:cache) { described_class.new(test_mode: true) }

  let(:key) { 'a/b/c' }
  let(:value) { Time.parse('2023-10-25T12:34:56Z') }
  let(:value_as_json) { '{"a/b/c":"2023-10-25T12:34:56Z"}' }

  before do
    allow(File).to receive_messages(exist?: true, read: value_as_json)
  end

  it 'uses its own cache file' do
    expect(described_class::CACHE_FILENAME).to eq('cache/SCAN_HISTORY.json')
  end

  it 'returns cached values as datetimes' do
    expect(cache.value_for(key)).to eq(value)
  end

  it 'returns nil if no value' do
    expect(cache.value_for('does-not-exist')).to be_nil
  end
end
