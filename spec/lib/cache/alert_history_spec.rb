# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Cache::AlertHistory do
  subject(:cache) { described_class.new(test_mode: true) }

  let(:key) { 'a/b/c!1' }
  let(:date) { Time.parse('2023-10-25T12:34:56Z') }
  let(:as_json) { '{"a/b/c!1": {"slack": "2023-10-25T12:34:56Z"}}' }

  before do
    allow(File).to receive_messages(exist?: true, read: as_json)
  end

  it 'uses its own cache file' do
    expect(described_class::CACHE_FILENAME).to eq('cache/ALERT_HISTORY.json')
  end

  describe '#alerted_at' do
    it 'returns cached values as datetimes' do
      expect(cache.alerted_at(key, 'slack')).to eq(date)
    end

    it 'returns nil if no value for mr reference' do
      expect(cache.alerted_at('does-not-exist', 'slack')).to be_nil
    end

    it 'returns nil if no value for output' do
      expect(cache.alerted_at(key, 'email')).to be_nil
    end
  end

  describe '#add_alert' do
    let(:anotherkey) { 'a/b/anotherproject!1' }
    let(:time) { Time.now.utc }
    let(:cached_time) { Time.parse(time.iso8601) }

    it 'creates a new cache entry if one does not exist' do
      cache.add_alert(anotherkey, 'slack', time)
      expect(cache.alerted_at(anotherkey, 'slack')).to eq(cached_time)
    end

    it 'adds to existing key if location does not exist' do
      cache.add_alert(key, 'email', time)
      expect(cache.alerted_at(key, 'email')).to eq(cached_time)
    end

    it 'updates existing key+location if one exists' do
      cache.add_alert(key, 'slack', time)
      expect(cache.alerted_at(key, 'slack')).to eq(cached_time)
    end
  end
end
