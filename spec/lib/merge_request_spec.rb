# frozen_string_literal: true

require 'spec_helper'

RSpec.describe MergeRequest do
  using RSpec::Parameterized::TableSyntax

  describe '.find_referenced_issue_in_source_branch' do
    let(:namespace) { 'group/project' }

    where(:source_branch, :expected_value) do
      [
        # Real examples from gitlab-org/gitlab
        ['420823-improve-number-formatting', [[ref(:namespace), 420_823]]],
        ['388725_timeout_pending_status', [[ref(:namespace), 388_725]]],
        ['ph/419862/widgetUpdate', [[ref(:namespace), 419_862]]],
        ['issue_410340-create_value_streams', [[ref(:namespace), 410_340]]],
        ['gitaly-ci-jobs-47435', [[ref(:namespace), 47_435]]],
        ['jj_393179_unify_epic_issue_args', [[ref(:namespace), 393_179]]],
        ['sh-fix-flaky-test-16-4', []],
        ['redis-sharedstate-multistore', []],
        # Fake examples for completeness
        ['20231010_mr_with_date', [[ref(:namespace), 20_231_010]]],
        ['foo-12345-67890$bar', [[ref(:namespace), 12_345]]],
        ['9999999999999999999', [[ref(:namespace), 9_999_999_999_999_999_999]]],
        ['99', []],
        ['001-foo', [[ref(:namespace), 1]]]
      ]
    end

    with_them do
      let(:mr) do
        described_class.new(
          Gitlab::ObjectifiedHash.new(
            source_branch:,
            references: {
              full: "#{namespace}!2"
            }
          )
        )
      end

      it 'detects an issue number when expected', :aggregate_failures do
        expect(mr.find_referenced_issue_in_source_branch).to eq(expected_value)
      end
    end
  end

  describe '.find_referenced_issues_in_description' do
    # Adapted from https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#default-closing-pattern
    let(:description) do
      %q(
This is an MR that does lots of things.

Fix #20, Fixes #21 and Closes group/otherproject#22.
This commit is also related to #23+.
and https://gitlab.example.com/group/project/-/issues/24
and https://gitlab.example.com/group/project/-/issues/25+
and https://gitlab.example.com/group/otherproject/issues/26
and https://gitlab.example.com/group/otherproject/issues/27+.

It also [fixes this](https://gitlab.example.com/group/project/issues/28) and
[fixes this](https://gitlab.example.com/group/anotherproject/issues/29).

It even relates to foo-bar_123.456/asd888#999.

Did I mention it fixes #20?

An unrelated URL is https://self-help.com/dealing-with/issues/1234.
[this](https://gitlab.acme.com/group/project/issues/2) is unrelated too.

This is not related to TODO item \#4.
)
    end
    let(:mr) do
      described_class.new(
        Gitlab::ObjectifiedHash.new(
          description:,
          references: {
            full: 'group/project!2'
          },
          web_url: 'https://gitlab.example.com/group/project/-/merge_requests/2'
        )
      )
    end

    let(:expected_values) do
      [
        ['group/project', 20],
        ['group/project', 21],
        ['group/otherproject', 22],
        ['group/project', 23],
        ['group/project', 24],
        ['group/project', 25],
        ['group/otherproject', 26],
        ['group/otherproject', 27],
        ['group/project', 28],
        ['group/anotherproject', 29],
        ['foo-bar_123.456/asd888', 999]
      ]
    end

    it 'extracts namespaced issue paths' do
      expect(mr.find_referenced_issues_in_description).to match_array(expected_values)
    end

    context 'with no references' do
      let(:description) { 'This is a simple MR.' }

      it 'extracts an empty array' do
        expect(mr.find_referenced_issues_in_description).to eq([])
      end
    end

    context 'with no description' do
      let(:description) { nil }

      it 'extracts an empty array' do
        expect(mr.find_referenced_issues_in_description).to eq([])
      end
    end

    context 'when the project is deeply nested' do
      let(:description) { 'This fixes #1 and baz/anotherproject#1 and anothergroup/secret#1.' }
      let(:mr) do
        described_class.new(
          Gitlab::ObjectifiedHash.new(
            description:,
            references: {
              full: 'abc/foo/bar/baz/project!1'
            },
            web_url: 'https://gitlab.example.com/abc/foo/bar/baz/project/-/merge_requests/1'
          )
        )
      end
      let(:expected_values) do
        [
          ['abc/foo/bar/baz/project', 1],
          ['abc/foo/bar/baz/anotherproject', 1],
          ['abc/foo/bar/anothergroup/secret', 1]
        ]
      end

      it 'applies the correct namespaces' do
        expect(mr.find_referenced_issues_in_description).to eq(expected_values)
      end
    end
  end

  describe '.find_referenced_confidential_issues' do
    let(:mr) { described_class.new({}) }
    let(:public_issue) { instance_double(Issue, 'confidential_security_issue?' => false) }
    let(:confidential_issue) { instance_double(Issue, 'confidential_security_issue?' => true) }

    before do
      allow(mr).to receive_messages(
        find_referenced_issue_in_source_branch: [['group/project', 1]],
        find_referenced_issues_in_description: [['another-group/project', 2]]
      )
      allow(Issue).to receive(:fetch).and_return(public_issue)
    end

    # rubocop:disable RSpec/MultipleExpectations
    it 'fetches the issues' do
      mr.find_referenced_confidential_issues

      expect(Issue).to have_received(:fetch).with('group/project', 1).ordered
      expect(Issue).to have_received(:fetch).with('another-group/project', 2).ordered
    end
    # rubocop:enable RSpec/MultipleExpectations

    it 'handles nil issues (errors)' do
      allow(Issue).to receive(:fetch).with('group/project', 1).and_return(nil)

      expect(mr.find_referenced_confidential_issues).to eq([])
    end

    it 'returns the issues that are confidential' do
      allow(Issue).to receive(:fetch).with('group/project', 1).and_return(confidential_issue)

      expect(mr.find_referenced_confidential_issues).to eq([confidential_issue])
    end
  end

  describe '.open_or_recent?' do
    where(:state, :merged_at, :closed_at, :expected_value) do
      now = Time.now.utc
      [
        ['open', nil, nil, true],
        ['merged', (now - MergeRequest::STALE_AGE + 60).iso8601, nil, true],
        ['merged', (now - MergeRequest::STALE_AGE).iso8601, nil, false],
        ['closed', nil, (now - MergeRequest::STALE_AGE + 60).iso8601, true],
        ['closed', nil, (now - MergeRequest::STALE_AGE).iso8601, false],
        ['locked', nil, nil, true]
      ]
    end

    with_them do
      let(:mr) do
        described_class.new(
          Gitlab::ObjectifiedHash.new(
            state:,
            merged_at:,
            closed_at:
          )
        )
      end

      it 'returns true when expected', :aggregate_failures do
        expect(mr.open_or_recent?).to eq(expected_value)
      end
    end
  end

  describe '.severity_from_labels' do
    where(:labels, :expected_value) do
      [
        [['bug::vulnerability', 'severity::1'], '1'],
        [['severity::99', 'severity::100'], '99'],
        [['severity:1'], nil],
        [['foobar'], nil],
        [[], nil]
      ]
    end

    with_them do
      let(:mr) do
        described_class.new(
          Gitlab::ObjectifiedHash.new(
            labels:
          )
        )
      end

      it 'returns expected severity', :aggregate_failures do
        expect(mr.severity_from_labels).to eq(expected_value)
      end
    end
  end

  describe 'label constants' do
    it 'has expected alert labels' do
      expect(described_class::ALERTED_LABELS).to eq(['bug::vulnerability'])
    end

    it 'has expected ignored labels' do
      expect(described_class::IGNORED_LABELS).to eq(['type::maintenance', 'security-fix-in-public'])
    end
  end

  describe '.alertworthy?' do
    where(
      :open_or_recent, :ignorable_labels, :alertable_labels, :find_referenced_confidential_issues, :expected_value
    ) do
      [
        [false, [],  [],  [],  false],
        [false, [1], [],  [],  false],
        [false, [1], [1], [],  false],
        [false, [1], [],  [1], false],
        [false, [1], [1], [1], false],
        [false, [],  [1], [],  false],
        [false, [],  [1], [1], false],
        [false, [],  [],  [1], false],

        [true, [],  [],  [],  false],
        [true, [1], [],  [],  false],
        [true, [1], [1], [],  false],
        [true, [1], [],  [1], false],
        [true, [1], [1], [1], false],
        [true, [],  [1], [],  true],
        [true, [],  [1], [1], true],
        [true, [],  [],  [1], true]
      ]
    end

    with_them do
      let(:mr) { described_class.new({}) }
      before do
        allow(mr).to receive_messages(
          open_or_recent?: open_or_recent,
          ignorable_labels:,
          alertable_labels:,
          find_referenced_confidential_issues:
        )
      end

      it 'alerts when expected', :aggregate_failures do
        expect(mr.alertworthy?).to eq(expected_value)
      end
    end
  end
end
