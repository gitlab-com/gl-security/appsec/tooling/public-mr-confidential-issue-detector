# frozen_string_literal: true

require 'spec_helper'

RSpec.describe DetectorService do
  let(:project_path) { 'example-group/example-project' }
  let(:options) do
    {
      domain: 'https://example.com',
      project_path:,
      since: Time.parse('2023-10-25T01:02:03Z'),
      outputs: %w[console slack]
    }
  end

  let(:updated_at) { '2023-10-25T12:34:56Z' }
  let(:alertworthy) { false }
  let(:mr) do
    instance_double(MergeRequest,
                    alertworthy?: alertworthy,
                    attributes: Gitlab::ObjectifiedHash.new(
                      updated_at:
                    ))
  end

  let(:gitlab_client) { instance_double(Gitlab::Client) }
  let(:cache) { instance_double(Cache::ScanHistory, add: false, flush: false, value_for: nil) }
  let(:response) { instance_double(Gitlab::PaginatedResponse) }

  before do
    allow(Gitlab::Client).to receive(:new).and_return(gitlab_client)
    allow(gitlab_client).to receive(:merge_requests).and_return(response)
    allow(response).to receive(:auto_paginate).and_yield({})
    allow(MergeRequest).to receive(:new).and_return(mr)
    allow(Cache::ScanHistory).to receive(:new).and_return(cache)

    allow(Alerter::Console).to receive(:alert)
    allow(Alerter::Slack).to receive(:alert)
  end

  describe '#run' do
    subject(:run) { described_class.run(options) }

    it 'outputs times to the console' do
      expect { run }
        .to output(a_string_including('Starting scan at '))
        .to_stdout
    end

    it 'raises no errors' do
      expect { run }.not_to raise_error
    end

    it 'updates the cache' do
      run
      expect(cache).to have_received(:add).with(options[:project_path], Time.parse(updated_at))
    end

    it 'flushes the cache' do
      run
      expect(cache).to have_received(:flush)
    end

    it 'fetches merge requests' do
      time = Time.now - (5 * 86_400)
      allow(described_class).to receive(:get_since_from_opts_or_cache!).and_return(time)
      run

      expect(gitlab_client).to have_received(:merge_requests).with(
        options[:project_path],
        {
          updated_after: time,
          per_page: 100,
          order_by: 'updated_at',
          sort: 'asc'
        }
      )
    end

    context 'with an alertworthy mr' do
      let(:alertworthy) { true }

      it 'triggers a console alert' do
        run
        expect(Alerter::Console).to have_received(:alert).with(mr)
      end

      it 'triggers a slack alert' do
        run
        expect(Alerter::Slack).to have_received(:alert).with(mr, anything)
      end
    end

    context 'with a not alertworthy mr' do
      let(:alertworthy) { false }

      it 'does not trigger a console alert' do
        run
        expect(Alerter::Console).not_to have_received(:alert)
      end

      it 'does not trigger a slack alert' do
        run
        expect(Alerter::Slack).not_to have_received(:alert)
      end
    end

    context 'with multiple projects' do
      let(:path_one) { 'example-group/example-project' }
      let(:path_two) { 'example-group/example-project-2' }
      let(:path_three) { 'example-group-2/example-project' }

      let(:project_path) { "#{path_one},#{path_two},#{path_three}" }

      it 'scans all of them' do # rubocop:disable RSpec/MultipleExpectations
        run
        expect(gitlab_client).to have_received(:merge_requests).with(path_one, anything).ordered
        expect(gitlab_client).to have_received(:merge_requests).with(path_two, anything).ordered
        expect(gitlab_client).to have_received(:merge_requests).with(path_three, anything).ordered
      end

      it 'caches all of them' do # rubocop:disable RSpec/MultipleExpectations
        run
        expect(cache).to have_received(:add).with(path_one, anything).ordered
        expect(cache).to have_received(:add).with(path_two, anything).ordered
        expect(cache).to have_received(:add).with(path_three, anything).ordered
      end
    end
  end

  describe '#get_since_from_opts_or_cache!' do
    subject(:get_since) { described_class.get_since_from_opts_or_cache!(project_path, options) }

    it 'checks the cache for the target project' do
      get_since
      expect(cache).to have_received(:value_for).with('example-group/example-project')
    end

    it 'uses options over cache if both are present' do
      allow(cache).to receive(:value_for).and_return(Time.now)
      get_since

      expect(get_since).to eq(options[:since])
    end

    context 'with no since argument' do
      let(:options) do
        {
          domain: 'https://example.com',
          project_path: 'example-group/example-project'
        }
      end

      it 'uses cache if present' do
        time = Time.now - (5 * 86_400)
        allow(cache).to receive(:value_for).and_return(time)

        expect(get_since).to eq(time)
      end

      it 'uses 30 days ago if no cache' do
        time = Time.now.utc
        expected = time - (30 * 86_400)
        expect(get_since.round).to eq(expected.round)
      end
    end
  end
end
