# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Alerter::Slack do
  let(:confidential_issues) { [] }
  let(:severity) { nil }
  let(:alertable_labels) { [] }
  let(:mr) do
    instance_double(MergeRequest,
                    find_referenced_confidential_issues: confidential_issues,
                    severity_from_labels: severity,
                    alertable_labels:,
                    attributes: Gitlab::ObjectifiedHash.new(
                      title: 'Example',
                      web_url: 'example.com/group/project/-/merge_requests/2',
                      created_at: '2023-10-18T22:22:50.726Z',
                      references: {
                        full: 'group/project!2'
                      },
                      author: {
                        username: 'danieljackson'
                      }
                    ))
  end

  describe '#slack_message_for_mr' do
    subject(:message) { described_class.slack_message_for_mr(mr) }

    shared_examples 'common message elements' do
      it 'includes the MR title' do
        expect(message.dig(:blocks, 0, :text, :text))
          .to match('Example')
      end

      it 'includes the URL' do
        expect(message.dig(:blocks, 0, :text, :text))
          .to match('example.com/group/project/-/merge_requests/2')
      end

      it 'does not include MR severity' do
        expect(message.dig(:blocks, 0, :text, :text))
          .not_to match('severity::')
      end

      context 'with severity' do
        let(:severity) { 99 }

        it 'includes severity label' do
          expect(message.dig(:blocks, 0, :text, :text))
            .to match('severity::99')
        end
      end
    end

    context 'when confidential issues are present' do
      let(:confidential_issue) do
        instance_double(Issue,
                        attributes: Gitlab::ObjectifiedHash.new(
                          title: 'Super serious vuln',
                          web_url: 'example.com/issue/1',
                          created_at: '2023-10-10T22:22:50.726Z',
                          confidential: true,
                          labels: ['severity::1', 'security', 'type::bug', 'bug::vulnerability'],
                          references: {
                            full: 'group/project#1'
                          }
                        ),
                        'vulnerability?' => true)
      end
      let(:confidential_issues) { [confidential_issue, confidential_issue] }

      # There are magic numbers in the digging through the ordered array.
      # If you reorder the Slack message content, the specs will need updating.

      include_examples 'common message elements'

      it 'includes confidential issue title' do
        expect(message.dig(:blocks, 1,  :text, :text))
          .to match('Super serious vuln')
      end

      it 'includes confidential issue URL' do
        expect(message.dig(:blocks, 1,  :text, :text))
          .to match('example.com/issue/1')
      end

      it 'is the expected number of blocks' do
        # MR, Issue, 3 Footers
        expect(message[:blocks].length).to eq(5)
      end

      it 'does not includes a message about alertable labels' do
        expect(message.dig(:blocks, 0,  :text, :text))
          .not_to include('labels!')
      end
    end

    context 'when alertable labels' do
      let(:alertable_labels) { ['bug::vulnerability'] }

      include_examples 'common message elements'

      it 'includes a message about alertable labels' do
        expect(message.dig(:blocks, 0,  :text, :text))
          .to include('It has `["bug::vulnerability"]` labels!')
      end

      it 'is the expected number of blocks' do
        # MR, 3 Footers
        expect(message[:blocks].length).to eq(4)
      end
    end
  end

  describe '#alert' do
    subject(:alert) { described_class.alert(mr, 'channel') }

    let(:client) { instance_double(Slack::Web::Client) }
    let(:channel) { 'channel' }
    let(:message) { { blocks: [{}] } }
    let(:cache) do
      instance_double(Cache::AlertHistory,
                      add: false,
                      flush: false,
                      value_for: nil,
                      add_alert: false,
                      alerted_at: nil)
    end

    before do
      allow(Slack::Web::Client).to receive(:new).and_return(client)
      allow(client).to receive(:chat_postMessage)
      allow(described_class).to receive(:slack_message_for_mr).and_return(message)
      allow(Cache::AlertHistory).to receive(:cache).and_return(cache)
    end

    it 'posts to a channel' do
      alert
      expect(client).to have_received(:chat_postMessage).with(
        channel:,
        blocks: message[:blocks],
        unfurl_links: false
      )
    end

    it 'retries errors' do
      allow_any_instance_of(Object).to receive(:sleep) # rubocop:disable RSpec/AnyInstance
      allow(client).to receive(:chat_postMessage).exactly(4).times.and_raise(StandardError.new)
      expect { alert }.to raise_error(StandardError)
    end

    context 'when alerts are cached' do
      it 'does not alert to slack if it has already' do
        allow(cache).to receive(:alerted_at).and_return(Time.now.utc)
        alert
        expect(client).not_to have_received(:chat_postMessage)
      end
    end
  end
end
