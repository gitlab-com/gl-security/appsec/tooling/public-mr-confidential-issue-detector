# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Alerter::Console do
  subject(:alert) { described_class.alert(mr) }

  let(:confidential_issues) { [] }
  let(:severity) { nil }
  let(:alertable_labels) { [] }
  let(:mr) do
    instance_double(MergeRequest,
                    find_referenced_confidential_issues: confidential_issues,
                    severity_from_labels: severity,
                    alertable_labels:,
                    attributes: Gitlab::ObjectifiedHash.new(
                      title: 'Example',
                      web_url: 'example.com',
                      created_at: '2023-10-18T22:22:50.726Z'
                    ))
  end

  describe '#alert' do
    it 'includes the title' do
      expect { alert }
        .to output(a_string_including('Example'))
        .to_stdout
    end

    it 'says when there are no confidential issues' do
      expect { alert }
        .to output(a_string_including('None detected'))
        .to_stdout
    end

    context 'when confidential issues are present' do
      let(:confidential_issues) do
        [
          instance_double(Issue,
                          attributes: Gitlab::ObjectifiedHash.new(
                            title: 'Super serious vuln',
                            web_url: 'example.com/issue/1',
                            confidential: true,
                            labels: ['severity::1', 'security', 'type::bug', 'bug::vulnerability']
                          ))
        ]
      end

      it 'includes confidential issue title' do
        expect { alert }
          .to output(a_string_including('Super serious vuln'))
          .to_stdout
      end

      it 'includes confidential issue url' do
        expect { alert }
          .to output(a_string_including('example.com/issue/1'))
          .to_stdout
      end

      it 'includes labels' do
        expect { alert }
          .to output(a_string_including('["severity::1", "security", "type::bug", "bug::vulnerability"]'))
          .to_stdout
      end
    end

    context 'when there is a severity' do
      let(:severity) { 99 }

      it 'includes it' do
        expect { alert }
          .to output(a_string_including('Severity: 99'))
          .to_stdout
      end
    end

    it "doesn't include the warning about alertworthy labels" do
      expect { alert }
        .not_to output(a_string_including('Alertworthy labels'))
        .to_stdout
    end

    context 'when it has alertworthy labels' do
      let(:alertable_labels) { ['bug::vulnerability'] }

      it 'warns about it' do
        expect { alert }
          .to output(a_string_including('Alertworthy labels: `["bug::vulnerability"]`'))
          .to_stdout
      end
    end
  end
end
