# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Issue do
  describe '#fetch!' do
    let(:gitlab_client) { instance_double(Gitlab::Client) }
    let(:issue) do
      Gitlab::ObjectifiedHash.new(
        id: 123,
        project_id: 456,
        title: 'Allow cool stuff',
        description: 'We should do a thing'
      )
    end

    before do
      allow(Gitlab::Client).to receive(:new).and_return(gitlab_client)
      allow(gitlab_client).to receive(:issue)
    end

    it 'fetches the issue from Gitlab' do
      described_class.fetch!('group/project', 1)
      expect(gitlab_client).to have_received(:issue).with('group/project', 1)
    end

    it 'creates a new issue with assigned attributes' do
      allow(gitlab_client).to receive(:issue).and_return(issue)

      subject = described_class.fetch!('group/project', 1)
      expect(subject.attributes.title).to eq('Allow cool stuff')
    end
  end

  describe '#fetch' do
    subject(:fetch) { described_class.fetch('group/project', 1) }

    it 'calls fetch!' do
      result = double
      allow(described_class).to receive(:fetch!).with('group/project', 1).and_return(result)
      expect(fetch).to eq(result)
    end

    it 'returns nil when error is raised' do
      # # error = Gitlab::Error::NotFound.new(
      # response =
      #   instance_double(HTTParty::Response,
      #     parsed_response: {},
      #     code: 404,
      #     request: instance_double(HTTParty::Request,
      #       base_uri: 'example.com',
      #       path: '/example-path'
      #     )
      #   )
      # # )
      # # allow(Gitlab::Error::NotFound).to receive(:new).and_return(error)
      # allow(Gitlab::Request).to receive(:get).and_raise(Gitlab::Error::NotFound, "foo")
      # # allow(gitlab_client).to receive(:get).and_return(response)
      # expect(fetch).to be_nil
      skip 'TODO: I spent too much time trying to mock this properly'
    end
  end

  describe '.confidential_security_issue?' do
    where(:confidential, :labels, :expected_value) do
      [
        [true, ['security'], true],
        [true, ['bug::vulnerability'], true],
        [true, ['security', 'type::feature'], true],
        [true, ['bug::availability'], false],
        [true, ['bug::vulnerability', 'security-fix-in-public'], false],
        [true, [], false],
        [false, ['security'], false],
        [false, ['bug::vulnerability'], false],
        [false, ['security', 'type::feature'], false],
        [false, [], false]
      ]
    end

    with_them do
      let(:mr) do
        described_class.new(
          Gitlab::ObjectifiedHash.new(
            confidential:,
            labels:
          )
        )
      end

      it 'returns true when expected', :aggregate_failures do
        expect(mr.confidential_security_issue?).to eq(expected_value)
      end
    end
  end

  describe '.vulnerability?' do
    where(:confidential, :labels, :expected_value) do
      [
        [true, ['security'], false],
        [true, ['bug::vulnerability'], true],
        [true, ['security', 'type::feature'], false],
        [true, ['bug::availability'], false],
        [true, ['bug::vulnerability', 'security-fix-in-public'], true],
        [true, [], false]
      ]
    end

    with_them do
      let(:mr) do
        described_class.new(
          Gitlab::ObjectifiedHash.new(
            confidential:,
            labels:
          )
        )
      end

      it 'returns true when expected', :aggregate_failures do
        expect(mr.vulnerability?).to eq(expected_value)
      end
    end
  end
end
