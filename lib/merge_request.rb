# frozen_string_literal: true

class MergeRequest
  attr_accessor :attributes, :referenced_issues

  STALE_AGE = (7 * 86_400) # Seven days in seconds

  # If any of these labels are present, ALWAYS ignore the MR
  IGNORED_LABELS = [
    'type::maintenance',
    'security-fix-in-public'
  ].freeze
  # If any of these labels are present (and we haven't
  # already ignored it), alert EVEN IF there are no
  # confidential issues detected
  ALERTED_LABELS = [
    'bug::vulnerability'
  ].freeze

  def initialize(attributes)
    self.attributes = attributes
  end

  def alertworthy?
    # Return early if we don't need to alert
    return false if !open_or_recent? || ignorable_labels.any?

    # Find confidential issues first, so they
    # can be included in the alert if present
    find_referenced_confidential_issues.any? ||
      alertable_labels.any?
  end

  # Identifies referenced issues and returns which, if any, are confidential
  # or inaccessible
  def find_referenced_confidential_issues
    @find_referenced_confidential_issues ||=
      begin
        namespace_and_ids = find_referenced_issue_in_source_branch + find_referenced_issues_in_description
        namespace_and_ids.filter_map do |namespace, id|
          issue = Issue.fetch(namespace, id)
          issue if issue&.confidential_security_issue?
        end
      end
  end

  def open_or_recent?
    stale = Time.now.utc - STALE_AGE
    case attributes.state
    when 'merged'
      stale < Time.parse(attributes.merged_at).utc
    when 'closed'
      stale < Time.parse(attributes.closed_at).utc
    else
      true
    end
  rescue StandardError
    true
  end

  def severity_from_labels
    attributes.labels.lazy.map { |l| l.scan(/severity::(\d+)/) }.find(&:first)&.first&.first
  end

  def alertable_labels
    ALERTED_LABELS.intersection(attributes.labels)
  end

  def ignorable_labels
    IGNORED_LABELS.intersection(attributes.labels)
  end

  # Returns a potential issue reference extracted from a branch
  # name. It might not be a real issue number at all.
  # Returns the value in the form [[namespace, integer]] to match
  #   #find_referenced_issues_in_description
  #
  # Uses the assumption that the referenced issues will all be > 99, and that
  # the first number in a branch name is the most likely one to be the issue
  # number.
  def find_referenced_issue_in_source_branch
    return [] unless (match_data = attributes.source_branch.match(/\d{3,}/))

    [[namespace, match_data[0].to_i]]
  end

  # Returns a set of issue details in the form [[namespace, id],...]
  def find_referenced_issues_in_description
    return [] if attributes.description.to_s.strip.empty?

    # Capture simple hash-style references
    hashes = attributes.description.scan(/(?<=^|\s)#(\d+)/)
    namespaced_references = hashes.collect do |match_data|
      [namespace, match_data[0].to_i]
    end

    # Capture namespaced hash references.
    # First, get any namespaces above the project's namespace.
    group_namespace = attributes.references.full.split('/')[0..-3]
    namespaced_references += attributes.description.scan(%r{(?<=^|\s)([a-zA-Z0-9\_\.\-]+/[a-zA-Z0-9\_\.\-]+)#(\d+)}).collect do |match_data|
      # The cross-project reference won't include higher namespaces (if present)
      # so we need to add those.
      cross_project_reference = match_data[0]
      cross_project_reference = "#{group_namespace.join('/')}/#{cross_project_reference}" if group_namespace.any?

      [cross_project_reference, match_data[1].to_i]
    end

    # Capture issue URLs that start with the expected base URL
    # We don't want to look for issue URLs that aren't on this MR's instance, so
    # we use `base_url` in the regex.
    namespaced_references += attributes.description.scan(%r{#{base_url}/([a-zA-Z0-9_\.\-/]+[a-zA-Z0-9]+)(?>/issues|/-/issues)/(\d+)}).collect do |match_data|
      [match_data[0], match_data[1].to_i]
    end

    namespaced_references.uniq
  end

  private

  def namespace
    # Get the namespace of this MR
    @namespace ||= attributes.references.full.match(%r{([a-zA-Z0-9_\.\-/]+)})[1]
  end

  def base_url
    @base_url ||= attributes.web_url.scan(%r{(http[s]?://[^/]*)/})[0][0]
  end
end
