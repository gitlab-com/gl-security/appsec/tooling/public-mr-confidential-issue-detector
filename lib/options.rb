# frozen_string_literal: true

require 'optparse'
require 'optparse/time'

class Options
  def self.parse!
    options = {
      domain: 'https://gitlab.com',
      outputs: []
    }

    optparse = OptionParser.new do |opts|
      opts.on('--project_path PROJECT_PATH', String, 'Full URL or <namespace/project> both accepted.') do |project_path|
        options[:project_path] = project_path.delete_prefix(options[:domain]).delete_suffix('/')
      end

      opts.on('--since DATE', Time, 'Search MRs created or updated after this UTC date') do |since|
        options[:since] = since
      end

      opts.on('--outputs console,slack', Array, 'List of locations to output') do |outputs|
        options[:outputs] = outputs
      end

      opts.on('--slack_channel channel_name_or_id', String, 'Slack channel to post alerts') do |slack_channel|
        options[:slack_channel] = slack_channel
      end

      opts.on('-h', '--help', 'Display this screen') do
        puts opts
        exit
      end
    end

    optparse.parse!

    puts 'WARN: GITLAB_API_PRIVATE_TOKEN was not provided as an ENV var'.colorize(:yellow) unless ENV['GITLAB_API_PRIVATE_TOKEN']&.length&.> 0

    puts 'WARN: No output locations chosen'.colorize(:yellow) unless options[:outputs].length.positive?

    unless options[:project_path]
      puts 'FATAL: --project_path must be provided.'.colorize(:red)
      exit 1
    end

    options
  end
end
