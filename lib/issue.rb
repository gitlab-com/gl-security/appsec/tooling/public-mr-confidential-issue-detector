# frozen_string_literal: true

class Issue
  VULN_LABEL = 'bug::vulnerability'
  SECURITY_LABELS = ['security', VULN_LABEL].freeze
  IGNORABLE_LABELS = ['security-fix-in-public'].freeze

  attr_accessor :attributes, :referenced_issues

  # Will raise an error if the Gitlab gem does
  def self.fetch!(namespace, id)
    new(Gitlab.issue(namespace, id))
  end

  # Ignores NotFound errors
  def self.fetch(namespace, id)
    fetch!(namespace, id)
  rescue Gitlab::Error::NotFound
    puts "WARNING: #{namespace}##{id} was not found. If the namespace exists, ensure the bot has access to it.".colorize(:yellow)
  end

  def initialize(attributes)
    self.attributes = attributes
  end

  def confidential_security_issue?
    attributes.confidential && attributes.labels.intersect?(SECURITY_LABELS) && !attributes.labels.intersect?(IGNORABLE_LABELS)
  end

  def vulnerability?
    attributes.labels.include? VULN_LABEL
  end
end
