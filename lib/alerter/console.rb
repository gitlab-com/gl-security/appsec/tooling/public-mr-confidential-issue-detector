# frozen_string_literal: true

require 'colorize'

module Alerter
  class Console
    def self.alert(merge_request)
      puts '=' * 40
      puts "Title:  #{merge_request.attributes.title}"
      puts "URL:    #{merge_request.attributes.web_url}"
      puts "Opened: #{merge_request.attributes.created_at}"
      puts "Severity: #{merge_request.severity_from_labels || 'Not present on MR'}"
      puts ''
      puts "Alertworthy labels: `#{merge_request.alertable_labels}`" if merge_request.alertable_labels.any?
      puts ''
      puts 'Confidential issues detected:'
      issues = merge_request.find_referenced_confidential_issues
      if issues.any?
        issues.each do |issue|
          puts "  Title:        #{issue.attributes.title}".colorize(:red)
          puts "  URL:          #{issue.attributes.web_url}"
          puts "  Confidential: #{issue.attributes.confidential}"
          puts "  Labels:       #{issue.attributes.labels}"
          puts ''
        end
      else
        puts '  None detected'.colorize(:green)
      end
    end
  end
end
