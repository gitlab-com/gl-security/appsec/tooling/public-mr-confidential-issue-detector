# frozen_string_literal: true

require 'gitlab'
require 'slack'
require 'colorize'
require 'require_all'

require_all File.dirname(__FILE__)

class DetectorService
  def self.run(options)
    Gitlab.configure do |config|
      config.endpoint = "#{options[:domain]}/api/v4"
    end
    if options[:outputs]&.include? 'slack'
      Slack.configure do |config|
        config.token = ENV.fetch(Alerter::Slack::TOKEN_ENV_VAR_NAME, nil)
      end
    end

    @count = 0
    scan_start_time = Time.now.utc
    puts "Starting scan at #{scan_start_time}."

    options[:project_path].split(',').each do |project_path|
      scan_project(project_path, options)
    end

    if ENV['DEBUG_CACHE']
      puts 'Alert cache file reads:'
      puts File.read(Cache::AlertHistory::CACHE_FILENAME) if File.exist?(Cache::AlertHistory::CACHE_FILENAME)
      puts 'Scan cache file reads:'
      puts File.read(Cache::ScanHistory::CACHE_FILENAME) if File.exist?(Cache::ScanHistory::CACHE_FILENAME)
    end

    finished_time = Time.now.utc
    elapsed_time = finished_time - scan_start_time

    puts "Done. Reviewed #{@count} merge requests in #{elapsed_time}s.".colorize(:green)
  end

  def self.scan_project(project_path, options)
    since = get_since_from_opts_or_cache!(project_path, options)
    puts "Scanning #{project_path} and outputting to #{options[:outputs]} for leaks since #{since}"

    Gitlab.merge_requests(
      project_path, {
        updated_after: since,
        per_page: 100,
        order_by: 'updated_at',
        sort: 'asc'
      }
    ).auto_paginate do |merge_request|
      mr = MergeRequest.new(merge_request)

      if mr.alertworthy?
        Alerter::Slack.alert(mr, options[:slack_channel]) if options[:outputs].include? 'slack'
        Alerter::Console.alert(mr) if options[:outputs].include? 'console'
      end

      print '.'

      updated = Time.parse(mr.attributes.updated_at)
      since = updated if updated > since

      @count += 1
    rescue StandardError => e
      puts 'ERROR: Encountered an error reviewing MR. Will attempt to update cache and exit. The merge request was:'.colorize(:red)
      puts merge_request&.to_h rescue StandardError # rubocop:disable Style/RescueModifier
      puts e
      puts e.backtrace
      break
    end
    puts ''

    puts "Caching #{since} for #{project_path}"
    Cache::ScanHistory.cache.add(project_path, since)
    Cache::ScanHistory.cache.flush
  rescue Gitlab::Error::NotFound => e
    puts "ERROR: Encountered an error fetching MRs for #{project_path}.".colorize(:red)
    puts e
    puts e.backtrace
  end

  def self.get_since_from_opts_or_cache!(path, options)
    since = Cache::ScanHistory.cache.value_for(path)
    if options[:since]
      puts "WARNING: --since for #{path} will override the value from the cache #{since}.".colorize(:yellow) unless since.nil?
      since = options[:since]
    end

    unless since
      puts "WARNING: --since was not provided and #{path} was not cached. Defaulting to 30 days ago.".colorize(:yellow)
      since = Time.now.utc - (30 * 86_400)
    end

    since
  end
end
