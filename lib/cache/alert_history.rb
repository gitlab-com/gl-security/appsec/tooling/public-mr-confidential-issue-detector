# frozen_string_literal: true

# Thanks, alphabet!
require_rel 'base'

module Cache
  class AlertHistory < Base
    CACHE_FILENAME = 'cache/ALERT_HISTORY.json'

    # Key should be an MR namespace, e.g. group/project!1
    # Location should be an output location, e.g. slack
    def alerted_at(key, location)
      return unless (value = value_for(key)) && value.key?(location)

      Time.parse(value[location])
    end

    def add_alert(key, location, time)
      value = value_for(key) || {}
      value[location] = time.utc.iso8601
      add(key, value)
    end
  end
end
