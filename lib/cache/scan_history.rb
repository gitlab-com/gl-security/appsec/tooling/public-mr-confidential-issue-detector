# frozen_string_literal: true

module Cache
  class ScanHistory < Base
    CACHE_FILENAME = 'cache/SCAN_HISTORY.json'

    # Key should be a project path, e.g. group/project
    def value_for(key)
      Time.parse(@cache[key]) if @cache.key?(key)
    end
  end
end
