# frozen_string_literal: true

require 'json'

module Cache
  class Base
    def self.cache(test_mode: false)
      @cache ||= new(test_mode:)
    end

    def initialize(test_mode: false)
      @test_mode = test_mode
      @cache = {}
      return unless File.exist?(self.class::CACHE_FILENAME)

      @cache = JSON.parse(File.read(self.class::CACHE_FILENAME))
    end

    def flush
      if @test_mode
        false
      else
        File.write(self.class::CACHE_FILENAME, @cache.to_json)
      end
    end

    def add(key, val)
      @cache[key] = val
    end

    def value_for(key)
      @cache[key]
    end
  end
end
